BootStrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version 5.2.1+singularity0

%files
    /src/clc-assembly-cell/5.2.1/clc-assembly-cell-5.2.1-linux_64.zip /opt
    /src/clc-assembly-cell/5.2.1/*.lic /opt

%post
    # build dependencies
    apt-get update -qq && apt-get install -y -qq \
        unzip \
    && rm -rf /var/lib/apt/lists/*

    cd /opt
    unzip -qq clc-assembly-cell-5.2.1-linux_64.zip
    mv /opt/*.lic /opt/clc-assembly-cell-5.2.1-linux_64/licenses/

    # cleanup
    rm -f clc-assembly-cell-5.2.1-linux_64.zip
    apt-get remove -y -qq unzip
    apt-get clean -qq

%environment
    export PATH=/opt/clc-assembly-cell-5.2.1-linux_64/:$PATH

%test
    export PATH=/opt/clc-assembly-cell-5.2.1-linux_64/:$PATH
    ls /opt/clc-assembly-cell-5.2.1-linux_64/licenses/*.lic
    clc_adapter_trim | grep "Version[:]* 5.2.1"
    clc_agp_join | grep "Version[:]* 5.2.1"
    clc_assembler | grep "Version[:]* 5.2.1"
    clc_assembler_long | grep "Version[:]* 5.2.1"
    clc_cas_to_sam | grep "Version[:]* 5.2.1"
    clc_change_cas_paths | grep "Version[:]* 5.2.1"
    clc_convert_sequences | grep "Version[:]* 5.2.1"
    clc_correct_pacbio_reads | grep "Version[:]* 5.2.1"
    clc_extract_consensus | grep "Version[:]* 5.2.1"
    clc_filter_matches | grep "Version[:]* 5.2.1"
    clc_join_mappings | grep "Version[:]* 5.2.1"
    clc_mapper | grep "Version[:]* 5.2.1"
    clc_mapper_legacy | grep "Version[:]* 5.2.1"
    clc_mapping_info | grep "Version[:]* 5.2.1"
    clc_mapping_table | grep "Version[:]* 5.2.1"
    clc_overlap_reads | grep "Version[:]* 5.2.1"
    clc_quality_trim | grep "Version[:]* 5.2.1"
    clc_remove_duplicates | grep "Version[:]* 5.2.1"
    clc_sample_reads | grep "Version[:]* 5.2.1"
    clc_sam_to_cas | grep "Version[:]* 5.2.1"
    clc_sequence_info | grep "Version[:]* 5.2.1"
    clc_sort_pairs | grep "Version[:]* 5.2.1"
    clc_split_reads | grep "Version[:]* 5.2.1"
    clc_submapping | grep "Version[:]* 5.2.1"
    clc_unmapped_reads | grep "Version[:]* 5.2.1"
    clc_unpaired_reads | grep "Version[:]* 5.2.1"
